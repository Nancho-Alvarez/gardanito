#!/usr/bin/env python3

import cairo
import gi
import random
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from gi.repository import Gdk


clefs={'G': u'\ue901', 'F': u'\ue904', 'C': u'\ue909'}
colors={'black': (0,0,0), 'blue': (0,0,1), 'white': (1,1,1)}
figures={
        'maxima':u'\ue95c',
        'longa':u'\ue95d',
        'breve':u'\ue95e',
        'semibreve':u'\ue93c',
        'minima':u'\ue95f',
        'seminima':u'\ue960',
        'fusa':u'\ue961'
        } 

font_point=24
staff_width=font_point/2
base=font_point*1.5
left_margin=10
step = font_point/8
scale=2
figure='semibreve'


class Line():
    def __init__(self):
        self.characters=[]
        self.characters.append(Symbol(clefs['G'], 2))
        self.characters.append(Symbol(figures[figure], 4))
        self.cursor=1
        self.set_cursor(self.cursor)

    def set_cursor(self, i):
        previous=self.characters[self.cursor]
        cursor=self.characters[i]
        previous.draw()
        cursor.draw('blue')
        self.cursor=i

class MyWindow(Gtk.Window):
    def __init__(self):
        super().__init__(title="Hola")
        self.connect('destroy', Gtk.main_quit)
        self.set_default_size(800, 800)
        self.darea=Gtk.DrawingArea()
        self.add(self.darea)
        self.darea.connect('draw', self.refresh_draw)
        self.set_focus()
        self.connect('key-press-event', self.on_key_press)
        self.show_all()

    def refresh_draw(self, da, cr):
        self.cr=cr
        pos=left_margin
        for i in line.characters:
            cr.set_source_surface(i.ims, pos, base)
            cr.paint()
            pos=pos + 2*i.width

    def on_key_press(self, widget, event):
        global figure
        global font_point
        global step
        global scale
        s=line.characters[line.cursor]
        if event.keyval==Gdk.KEY_Up:
            s.height+=1
            s.draw(color='blue')
        elif event.keyval==Gdk.KEY_Down:
            s.height-=1
            s.draw(color='blue')
        elif event.keyval==Gdk.KEY_Left:
            if line.cursor>0:
                line.set_cursor(line.cursor-1)
        elif event.keyval==Gdk.KEY_Right:
            if line.cursor<len(line.characters)-1:
                line.set_cursor(line.cursor+1)
        elif event.keyval==Gdk.KEY_Delete or event.keyval==Gdk.KEY_KP_Delete: 
            line.set_cursor(line.cursor-1)
            del line.characters[line.cursor+1]
        elif event.keyval==Gdk.KEY_BackSpace: 
            line.set_cursor(line.cursor-1)
            del line.characters[line.cursor]
            s.draw(color='blue')
        elif event.keyval==Gdk.KEY_Insert or event.keyval==Gdk.KEY_KP_Insert:
            line.characters.insert(
                    line.cursor, Symbol(figures[figure], s.height))
            line.cursor+=1
            line.set_cursor(line.cursor-1)
        elif event.keyval==Gdk.KEY_1:
            figure='maxima'
            s.char=figures['maxima']
            self.add_note()
        elif event.keyval==Gdk.KEY_2:
            figure='longa'
            s.char=figures['longa']
            self.add_note()
        elif event.keyval==Gdk.KEY_3:
            figure='breve'
            s.char=figures['breve']
            self.add_note()
        elif event.keyval==Gdk.KEY_4:
            figure='semibreve'
            s.char=figures['semibreve']
            self.add_note()
        elif event.keyval==Gdk.KEY_5:
            figure='minima'
            s.char=figures['minima']
            self.add_note()
        elif event.keyval==Gdk.KEY_6:
            figure='seminima'
            s.char=figures['seminima']
            self.add_note()
        elif event.keyval==Gdk.KEY_7:
            figure='fusa'
            s.char=figures['fusa']
            self.add_note()
        elif event.keyval==Gdk.KEY_q:
            Gtk.main_quit()
        self.darea.queue_draw()

    def add_note(self):
        s=line.characters[line.cursor]
        if line.cursor==len(line.characters)-1:
            line.characters.append(Symbol(figures[figure], s.height))
            line.set_cursor(line.cursor+1)
        else:
            line.set_cursor(line.cursor)


class Symbol:
    def __init__(self, char, height):
        self.char=char
        self.height=height
        self.color='black'
        self.ims=cairo.ImageSurface(cairo.FORMAT_ARGB32, 500,500)
        self.draw()

    def draw(self, color=None):
        if color == None:
            color=self.color
        cr=cairo.Context(self.ims)
        cr.set_source_rgb(*colors['white'])
        cr.paint()
        #cr.set_source_rgb(*colors['black'])
        cr.set_source_rgb(*colors[color])
        cr.scale(scale,scale)
        cr.select_font_face("Bravura")
        cr.set_font_size(font_point)
        cr.set_line_width(font_point/32)
        _, _, width, _, _, _ = cr.text_extents(self.char)
        self.x=min(width/2, font_point/8)
        self.xx=self.x
        # self.x  is the staff space to the left  of the symbol
        # self.xx is the staff space to the right of the symbol
        sw=self.x + width + self.xx
        self.y=-step*self.height
        for i in range(5):
            cr.move_to(0, base - 2*step*i)
            cr.line_to(sw, base -2*step*i)
            cr.stroke()
        #cr.set_source_rgb(*colors[color])
        cr.move_to(0, base)
        cr.rel_move_to(self.x, self.y)
        cr.show_text(self.char)
        self.width=sw


line=Line()
win = MyWindow()
Gtk.main()
